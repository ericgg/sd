import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client{

	public static void main(String args[]) throws Exception{
	   String host = args[0];//primeiro argumento é o ip
	   int port = Integer.parseInt(args[1]);// o segundo argumento é a porta
	

	Socket sck = new Socket(host,port);//abrimos um Socket com o ip e a porta. Nosso destinatario.

	ObjectOutputStream out = new ObjectOutputStream(sck.getOutputStream());//Abrimos o fluxo de envio no socket que usamos (sck)
	out.flush();

	ObjectInputStream in = new ObjectInputStream(sck.getInputStream());

	Scanner s = new Scanner(System.in);
	String mensagem = "";


	do{
	mensagem = (String) in.readObject();
	System.out.println(mensagem);//Aviso pro cliente
	mensagem = s.nextLine();
	out.writeObject(mensagem);
	out.flush();
	}while(!mensagem.equals("FIM"));


	sck.close();//Fecha o socket
}
}
